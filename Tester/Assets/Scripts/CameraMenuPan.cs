﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMenuPan : MonoBehaviour {
    public bool zooming;
    public float zoomSpeed;
	public Camera cameramain;

	//Change based on how much you want to scroll
	public float countdown = 30f;
    //commit purposes
	//Change based on how fast you want to scroll
	public float cameraMoveSpeed = 0.18f;


    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
      
		//change based on how much you want to scroll (countdown)
		//if countdown is between a certain range, move camera a certain direction, when it goes back to the bottom, reset countdown

		//TODO Find a better way for countdown yes
		if (countdown >= 16) {
			cameramain.transform.position += new Vector3 (0, 1 * (Time.deltaTime * cameraMoveSpeed), 0);
			countdown -= Time.deltaTime;
		} else if (countdown >= 2 && countdown < 16) {
			cameramain.transform.position -= new Vector3 (0, 1 * (Time.deltaTime * cameraMoveSpeed), 0);
			countdown -= Time.deltaTime;
		} else {
			countdown = 30;
		}
	}
}
    
