﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayer : MonoBehaviour {
    private GameObject player;
    public float minx;
    public float maxx;
    public float miny;
    public float maxy;
	// Use this for initialization pls
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");


		minx = -15;
		maxx = 30;

		miny = -9;
		maxy = 7;
    }

	//commit purposes
	// Update is called once per frame
	void LateUpdate () {
		float x = Mathf.Clamp(player.transform.position.x, minx, maxx);
        float y = Mathf.Clamp(player.transform.position.y, miny, maxy);
        //gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
		transform.position = new Vector3 (x, y, gameObject.transform.position.z);
    }

}
