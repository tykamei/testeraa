﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float playerSpeed = 15;
    public int playerJumpPower = 10;
    public float moveX;
    public bool isGround;
    public int checker = 1;
    public int jumpTime = 0;

	private float minx;
	private float maxx;
	private float maxy;
    // Update is called once per frame mhmm
    void Start()
    {
		minx = -45;
		maxx = 59;

		maxy = 9;
    }
    void Update()
    {
        
        PlayerMove();
        checker++;
        if (Input.GetButton("Jump"))
        {
            jumpTime++;
        }
        else
        {
            jumpTime = 0;
        }
    }
        
    void PlayerMove()
    {
        if (Input.GetButton("Jump") && checker > 30)
        {
            Jump();
            checker = 0;
        }
        
        moveX = Input.GetAxis("Horizontal");

		//CONFINES PLAYER TO STAGE BOUNDARIES
		if (transform.position.x < minx) {
			transform.position = new Vector3(minx, transform.position.y, transform.position.z);
		}
		if (transform.position.x > maxx) {
			transform.position = new Vector3 (maxx, transform.position.y, transform.position.z);
		}

		//PLAYER MOVEMENT
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);




        
        
    }
    void Jump()
    {
        
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        if (jumpTime > 30)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 3);
        }
    }
	//commit purposes
    private void IsTouching(Collider2D collider)
    {
		isGround = true;
        


    }



}
